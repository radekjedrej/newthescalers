<?php

$title = get_sub_field("title") ? get_sub_field("title") : "";
$subtitle = get_sub_field("subtitle") ? get_sub_field("subtitle") : "";
$text = get_sub_field("text") ? get_sub_field("text") : "";
$button = get_sub_field("button") ? get_sub_field("button") : "";
$image_bg = get_sub_field("image") ? get_sub_field("image") : "";
$image_bg_mobile = get_sub_field("image_mobile") ? get_sub_field("image_mobile") : "";

?>

<section class="edge-full">
    <div class="main-wrapper">
        <div class="edge-full__list">
            <div class="edge-full__intro u-text-center">
                <h3 class="text-describe color-blue f-400">Trusted by great companies worldwide:</h3>
            </div>
    
            <?php if( have_rows('companies_group') ): ?>  
                <div class="edge-full__container">
                    <ul class="edge-full__ul list-reset swiper-wrapper">
    
                    <?php while( have_rows('companies_group') ) : the_row();
                    $image = get_sub_field('image') ?  get_sub_field('image') : '';    
                    ?>
                    <li class="edge-full__li swiper-slide"><img class="edge-full__logo" src="<?= $image['url'] ?>" alt="<?= $image['alt'] ?>"></li>
                    <?php endwhile; ?>

                    </ul>
                </div>  
            <?php endif ?>

        </div>
    </div>
    
    <div class="edge-full__content__wrapper">
        <div class="main-wrapper">
            <div class="edge-full__content">
                <div class="edge-full__box">
                    <?php if($subtitle): ?>
                    <div class="edge-full__subtitle text-intro f-600"><?= $subtitle ?></div>
                    <?php endif ?>
                    <?php if($title): ?>
                    <h3 class="edge-full__title display-2"><?= $title ?></h3>
                    <?php endif ?>
                    <?php if($text): ?>
                    <div class="edge-full__text">
                        <?= $text ?>
                    </div>
                    <?php endif ?>
                    <?php if($button): ?>
                    <a class="u-btn--black" href="<?= $button['url'] ?>" target="<?= $button['target'] ?>"><?= $button['title'] ?></a>
                    <?php endif ?>
                </div>
            </div>
        </div>   
    </div>
    
    <div class="edge-full__content__bg">
        <img class="edge-full__content__img" src="<?= $image_bg['url'] ?>" alt="<?= $image_bg['alt'] ?>">
    </div>

    <div class="edge-full__content__bg--mobile">
        <img class="edge-full__content__img--mobile" src="<?= $image_bg_mobile['url'] ?>" alt="<?= $image_bg_mobile['alt'] ?>">
    </div>

    <div class="edge-full__content__wave">
        <?php include get_icons_directory('svg/wave-edge.svg') ?>
    </div>

</section>