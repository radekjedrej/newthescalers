import './js/navigation';
import './js/skip-link-focus-fix';

import { vh, handleWindow } from './js/helpers'
import { SwiperCarousel } from "./js/swiper"

window.addEventListener("DOMContentLoaded", () => {

    handleWindow()
    new SwiperCarousel();
})