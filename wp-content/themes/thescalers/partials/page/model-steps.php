<?php if( have_rows('steps_group') ): 
$count = 1;    
?>  
    <section class="model-steps__wrapper">
        <div class="main-wrapper">
            <div class="model-steps">
            
            <?php while( have_rows('steps_group') ) : the_row();
            $image = get_sub_field('image') ?  get_sub_field('image') : '';  
            $title = get_sub_field('title') ?  get_sub_field('title') : '';  
            $text = get_sub_field('text') ?  get_sub_field('text') : '';  
            $button = get_sub_field('button') ?  get_sub_field('button') : '';  
            ?>

            <div class="model-steps__box">
                <div class="model-steps__content">

                    <div class="model-steps__bg">
                        <?php include get_icons_directory('svg/stepsbg.svg') ?>
                    </div>

                    <div class="model-steps__image">
                        <?php if($image['url']): ?>
                        <img class="model-steps__img img-fluid" src="<?= $image['url'] ?>" alt="<?= $image['alt'] ?>">
                        <?php endif; ?>
                    </div>
         
                    <div class="model-steps__content__split model-steps__content__split--<?= $count ?>">
                        <div class="model-steps__number">
                            <div class="model-steps__num model-steps__num--<?= $count ?> d-flex d-flex-center f-heading f-800">0<?= $count ?></div>
                        </div>
    
                        <div class="model-steps__copy">
                            <?php if($title): ?>
                            <h4 class="model-steps__title display-5"><?= $title ?></h4>
                            <?php endif; ?>
                            <?php if($text): ?>
                            <p class="model-steps__text line-half"><?= $text ?></p>
                            <?php endif; ?>
                        </div>
    
                        <?php if($button['url']): ?>
                        <div class="model-steps__button--mobile">
                            <a href="<?= $button['url'] ?>" target="<?= $button['target'] ?>" class="u-btn--white"><?= $button['title'] ?></a>
                        </div>
                        <?php endif; ?>
                    </div>
                </div>
                
                <?php if($button['url']): ?>
                <div class="model-steps__button">
                    <a href="<?= $button['url'] ?>" target="<?= $button['target'] ?>" class="u-btn--white"><?= $button['title'] ?></a>
                </div>
                <?php endif; ?>

            </div>

            <?php 
            $count++;
            endwhile; ?>
            
            </div>
        </div>
    </section>
<?php endif ?>
