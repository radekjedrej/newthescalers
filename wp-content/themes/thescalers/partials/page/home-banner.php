<?php

$image = get_sub_field("image") ? get_sub_field("image") : "";
$title = get_sub_field("title") ? get_sub_field("title") : "";
$button = get_sub_field("button") ? get_sub_field("button") : "";

?>

<section class="home-banner">
    <div class="main-wrapper">
        <div class="home-banner__wrapper">
            <div class="home-banner__inner d-flex d-flex-wrap">
                <div class="home-banner__content">
                    <?php if($title): ?>
                    <h2 class="home-banner__title display-1"><?= $title ?></h2>
                    <?php endif ?>
    
                    <?php if( have_rows('list_group') ): ?>    
                        <ul class="home-banner__ul list-reset">
                        <?php while( have_rows('list_group') ) : the_row();
                            
                        $item = get_sub_field('item');    
                        ?>
    
                        <li class="home-banner__li text-list d-flex d-flex-align-c"><?php include get_icons_directory('icons/tick_small.svg') ?> <?= $item ?></li>
    
                        <?php endwhile; ?>
                        </ul>
    
                        <?php if($button['title']): ?>
                        <div class="home-banner__cta">
                            <a class="u-btn--black" href="<?= $button['url'] ?>" target="<?= $button['target'] ?>"><?= $button['title'] ?></a>
                        </div>
                        <?php endif ?>
    
                    <?php endif; ?>
                </div>
            </div>
    
            <div class="home-banner__bg">
                <div class="home-banner__mask__one"><?php include get_icons_directory('svg/banner-mask-1.svg') ?></div>
                <img class="home-banner__img d-block" src="<?= $image['url'] ?>" alt="<?= $image['alt'] ?>">
                <div class="home-banner__mask__two"><?php include get_icons_directory('svg/banner-mask-2.svg') ?></div>
                <div class="home-banner__mask__three"><?php include get_icons_directory('svg/banner-mask-3.svg') ?></div>
            </div>
        </div>
    </div>
</section>
