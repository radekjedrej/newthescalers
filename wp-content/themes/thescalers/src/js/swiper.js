import Swiper, { Navigation, Autoplay } from 'swiper'  

export class SwiperCarousel {
  constructor() {
    Swiper.use([Navigation, Autoplay]);

    this.companies
    this.init()
  }

  init() {
    this.companiesLogos();
  }

  companiesLogos() {
    this.companies = new Swiper(".edge-full__container", {
      slidesPerView: 2,
      loop: true,
      autoplay: true,
      freeMode: true,
      breakpoints: {
        768: {
          slidesPerView: 3
        },
        1280: {
          autoplay: false,
          loop: false,
          slidesPerView: 5,
          resistanceRatio: 0
        }
      }
    });
  }
}